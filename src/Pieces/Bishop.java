/**
 * 
 * 
 * @author il166, yuc1
 * 
 * 
 * 
 */

package Pieces;

import java.util.ArrayList;

import board.*;

public class Bishop extends ChessPiece{
	
	/**
	 * 
	 * @param color of the piece
	 * @param row of the piece 
	 * @param column of the piece 
	 */
	public Bishop(char color, int row, int column){
		super(row,column,color);
		
		
	}
	
	/**
	 * ToStrings the object Bishop
	 * @return String of the object
	 */
	public String toString() {
		return super.getColor()+"B";
		
	}
	
	/**
	 * Gets a list of all the possible moves that bishop can make. 
	 * @return ArrayList<int[]> of all possible moves that piece can do. 
	 */
	@Override
	public ArrayList<int[]> validMoves(){
				
		int[] initialPos = super.getPosition();
		
		ArrayList<int[]> listOfMoves = new ArrayList<>();
		
		int row= initialPos[0];
		int column=initialPos[1];
		int currentRow, currentColumn;

		//check to the left of piece, and go down.
		for(currentRow=row+1 ,currentColumn=column-1;currentColumn>=0 && currentRow<=7; currentColumn--, currentRow++) {
			
			ChessPiece pieceAtLocation=Board.getPieceAtLocation(new int[] {currentRow,currentColumn});
				
			if(pieceAtLocation==null){
				listOfMoves.add(new int[] {currentRow, currentColumn});
			}else if(!(pieceAtLocation.getColor()==(super.getColor()))) {
				listOfMoves.add(new int[] {currentRow, currentColumn});
				break;
			}else {
				break;
			}
			
		}
		
		//check to the left of piece and go up.
		for(currentRow=row-1 ,currentColumn=column-1;currentColumn>=0 && currentRow>=0; currentColumn--, currentRow--) {

			ChessPiece pieceAtLocation=Board.getPieceAtLocation(new int[] {currentRow,currentColumn});
				
			if(pieceAtLocation==null){
				listOfMoves.add(new int[] {currentRow, currentColumn});
			}else if(!(pieceAtLocation.getColor()==(super.getColor()))) {
				listOfMoves.add(new int[] {currentRow, currentColumn});
				break;
			}else {
				break;
			}
		}
		
		//check to the right of piece and go up.
		for(currentRow=row-1 ,currentColumn=column+1;currentColumn<=7 && currentRow>=0; currentColumn++, currentRow--) {

			ChessPiece pieceAtLocation=Board.getPieceAtLocation(new int[] {currentRow,currentColumn});
				
			if(pieceAtLocation==null){
				listOfMoves.add(new int[] {currentRow, currentColumn});
			}else if(!(pieceAtLocation.getColor()==(super.getColor()))) {
				listOfMoves.add(new int[] {currentRow, currentColumn});
				break;
			}else {
				break;
			}
		}
		
		//check to the right of piece and go down.
		for(currentRow=row+1 ,currentColumn=column+1;currentColumn<=7 && currentRow<=7; currentColumn++, currentRow++) {

			ChessPiece pieceAtLocation=Board.getPieceAtLocation(new int[] {currentRow,currentColumn});
				
			if(pieceAtLocation==null){
				listOfMoves.add(new int[] {currentRow, currentColumn});
			}else if(!(pieceAtLocation.getColor()==(super.getColor()))) {
				listOfMoves.add(new int[] {currentRow, currentColumn});
				break;
			}else {
				break;
			}
		}

		return listOfMoves;
		
	}

}
