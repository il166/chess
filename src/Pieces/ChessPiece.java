
package Pieces;
import java.util.ArrayList;

public abstract class ChessPiece {
	
	private int row;
	private int column;
	private char color;
	
	/**
	 * Constructor for Chess Piece Class
	 * @author il166, yuc1
	 * @param row of the piece
	 * @param column of the piece
	 * @param color of the piece
	 */
	public ChessPiece(int row, int column, char color) {

		this.color = color;
		this.row=row;
		this.column=column;
		
	}
	
	/**
	 * Gets the Position of the chess piece and returns it. 
	 * @return int[] returns the position of the piece as an int array.
	 */
	public int[] getPosition() {
	
		return new int[] {row,column};
	}
	
	/**
	 * Takes in a int array and updates the position of the piece.
	 * @param newPos new position to move the piece to. 
	 */
	public void updatePosition(int[] newPos) {
		row=newPos[0];
		column=newPos[1];
	}
	
	/**
	 * returns a character for the color of the piece. 
	 * @return char color of the piece. 
	 */
	public char getColor() {
		return this.color;
	}

	/**
	 * Abstract method for valid moves. 
	 * @return ArrayList of ints of all the possible moves. 
	 */
	public abstract ArrayList<int[]> validMoves();

	
		
}
