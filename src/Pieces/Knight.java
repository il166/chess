/**
 * 
 * 
 * @author il166, yuc1
 * 
 * 
 * 
 */

package Pieces;

import java.util.ArrayList;

import board.Board;

public class Knight extends ChessPiece{
	
	/**
	 * Constructor for the Knight. 
	 * 
	 * @param c color of the piece
	 * @param row of the piece
	 * @param column of the piece
	 */
	public Knight(char c, int row, int column){
		super(row,column, c);
		
	}
	
	/**
	 * ToStrings the object Knight
	 * @return String of the object
	 */
	public String toString() {
		return super.getColor()+"N";
		
	}

	/**
	 * Gets a list of all the possible moves that knight can make. 
	 * @return ArrayList<int[]> of all possible moves that piece can do. 
	 */
	@Override
	public ArrayList<int[]> validMoves() {
		
		
		int[] initialPos= super.getPosition();
		
		ArrayList<int[]> listOfMoves = new ArrayList<>();
		
		int row= initialPos[0];
		int column=initialPos[1];
		ChessPiece piece;
		
		//up two left one
		if(row-2>=0 && column-1>=0) {
			piece = Board.getPieceAtLocation(new int[] { row-2, column-1});
			if ( piece ==null || piece.getColor()!=super.getColor()) {
				listOfMoves.add(new int[] {row-2, column-1});
			}
		}
		
		//up two right one
		if(row-2>=0 && column+1<=7) {
			piece = Board.getPieceAtLocation(new int[] { row-2, column+1});
			if ( piece ==null || piece.getColor()!=super.getColor()) {
				listOfMoves.add(new int[] {row-2, column+1});
			}
		}
		
			
		//up one left two
		if(row-1>=0 && column-2>=0) {
			piece = Board.getPieceAtLocation(new int[] { row-1, column-2});
			if ( piece ==null || piece.getColor()!=super.getColor()) {
				listOfMoves.add(new int[] {row-1, column-2});
			}
		}	
			
		//up one right two
		if(row-1>=0 && column+2<=7) {
			piece = Board.getPieceAtLocation(new int[] { row-1, column+2});
			if ( piece ==null || piece.getColor()!=super.getColor()) {
				listOfMoves.add(new int[] {row-1, column+2});
			}
		}		
			
		//down two left one
		if(row+2<=7 && column-1>=0) {
			piece = Board.getPieceAtLocation(new int[] { row+2, column-1});
			if ( piece ==null || piece.getColor()!=super.getColor()) {
				listOfMoves.add(new int[] {row+2, column-1});
			}
		}	
		
		//down two right one
		if(row+2<=7 && column+1<=7) {
			piece = Board.getPieceAtLocation(new int[] { row+2, column+1});
			if ( piece ==null || piece.getColor()!=super.getColor()) {
				listOfMoves.add(new int[] {row+2, column+1});
			}
		}	
		
		//down one right two
		if(row+1<=7 && column+2<=7) {
			piece = Board.getPieceAtLocation(new int[] { row+1, column+2});
			if ( piece ==null || piece.getColor()!=super.getColor()) {
				listOfMoves.add(new int[] {row+1, column+2});
			}
		}	
		
		//down one left two
		if(row+1<=7 && column-2>=0) {
			piece = Board.getPieceAtLocation(new int[] { row+1, column-2});
			if ( piece ==null || piece.getColor()!=super.getColor()) {
				listOfMoves.add(new int[] {row+1, column-2});
			}
		}	
		
		return listOfMoves;
	}
	
	

}
