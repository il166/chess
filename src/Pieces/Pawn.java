/**
 * 
 * 
 * @author il166, yuc1
 * 
 * 
 * 
 */

package Pieces;

import java.util.ArrayList;

import board.*;

public class Pawn extends ChessPiece{

	public Pawn(char color, int row, int column){
		super(row, column, color);
	}

	/**
	 * ToStrings the object Pawn
	 * @return String of the object
	 */
	public String toString() {
		return super.getColor()+"p";

		
	}
	
	
	/**
	 * Gets a list of all the possible moves that pawn can make. 
	 * @return ArrayList<int[]> of all possible moves that piece can do. 
	 */
	@Override
	public ArrayList<int[]> validMoves() {
		
		ArrayList<int[]> possibleMoves = new ArrayList<int[]>();
		
		int row = super.getPosition()[0];
		int column = super.getPosition()[1];
		
		int[] possibleSpot = new int[2];	
		possibleSpot[0] = row;
		
		
		
		if(super.getColor() == 'b') {
			
			if(row == 1) {
				
				possibleSpot[0] = row + 1;
				possibleSpot[1] = column;
				if(Board.getPieceAtLocation(possibleSpot) == null) {
					//spot in front is available
					possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
					possibleSpot[0] = row + 2;
					//spot 2 spaces in front is available
					if(Board.getPieceAtLocation(possibleSpot) == null) possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
				}
				if(column == 0) {
					possibleSpot[0] = row + 1;
					possibleSpot[1] = column + 1;
					if(Board.getPieceAtLocation(possibleSpot) != null && Board.getPieceAtLocation(possibleSpot).getColor() == 'w') 
						possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
				}
				//pawn is all the way right
				else if(column == 7) {
					possibleSpot[0] = row + 1;
					possibleSpot[1] = column - 1;
					if(Board.getPieceAtLocation(possibleSpot) != null && Board.getPieceAtLocation(possibleSpot).getColor() == 'w') 
						possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
				}
				else if(column < 7 && column > 0) {
					possibleSpot[0] = row + 1;
					
					possibleSpot[1] = column + 1;
					if(Board.getPieceAtLocation(possibleSpot) != null && Board.getPieceAtLocation(possibleSpot).getColor() == 'w') possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
					
					possibleSpot[1] = column - 1;
					if(Board.getPieceAtLocation(possibleSpot) != null && Board.getPieceAtLocation(possibleSpot).getColor() == 'w') possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
					
				}
				
				
			}
			else {
				possibleSpot[0] = row + 1;
				possibleSpot[1] = column;
				
				//spot in front is available
				if(Board.getPieceAtLocation(possibleSpot) == null) possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
				
				//Handle Diagonals
				
				//pawn is all the way left
				if(column == 0) {
					possibleSpot[0] = row + 1;
					possibleSpot[1] = column + 1;
					if(Board.getPieceAtLocation(possibleSpot) != null && Board.getPieceAtLocation(possibleSpot).getColor() == 'w') 
						possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
				}
				//pawn is all the way right
				else if(column == 7) {
					possibleSpot[0] = row + 1;
					possibleSpot[1] = column - 1;
					if(Board.getPieceAtLocation(possibleSpot) != null && Board.getPieceAtLocation(possibleSpot).getColor() == 'w') 
						possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
				}
				//in the middle somewhere
				else if(column < 7 && column > 0) {
					possibleSpot[0] = row + 1;
					
					possibleSpot[1] = column + 1;
					if(Board.getPieceAtLocation(possibleSpot) != null && Board.getPieceAtLocation(possibleSpot).getColor() == 'w') possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
					
					possibleSpot[1] = column - 1;
					if(Board.getPieceAtLocation(possibleSpot) != null && Board.getPieceAtLocation(possibleSpot).getColor() == 'w') possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
					
				}
				
			}
		}
		
		
		if(super.getColor() == 'w') {
			
			if(row == 6) {
				
				possibleSpot[0] = row - 1;
				possibleSpot[1] = column;
				
				if(Board.getPieceAtLocation(possibleSpot) == null) {
					//spot in front is available
					possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
					
					possibleSpot[0] = row - 2;
					//spot 2 spaces in front is available
					if(Board.getPieceAtLocation(possibleSpot) == null) {
						possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
					}
				}
				
				
				if(column == 0) {
					possibleSpot[0] = row - 1;
					possibleSpot[1] = column + 1;
					if(Board.getPieceAtLocation(possibleSpot) != null && Board.getPieceAtLocation(possibleSpot).getColor() == 'b') 
						possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
				}
				//pawn is all the way right
				else if(column == 7) {
					possibleSpot[0] = row - 1;
					possibleSpot[1] = column - 1;
					if(Board.getPieceAtLocation(possibleSpot) != null && Board.getPieceAtLocation(possibleSpot).getColor() == 'b') 
						possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
				}
				//in the middle somewhere
				else if(column < 7 && column > 0) {
					possibleSpot[0] = row - 1;
					
					possibleSpot[1] = column + 1;
					if(Board.getPieceAtLocation(possibleSpot) != null && Board.getPieceAtLocation(possibleSpot).getColor() == 'b') possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
					
					possibleSpot[1] = column - 1;
					if(Board.getPieceAtLocation(possibleSpot) != null && Board.getPieceAtLocation(possibleSpot).getColor() == 'b') possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
					
				}
				
			}
			else {
				
				
				possibleSpot[0] = row -  1;
				possibleSpot[1] = column;
				//spot in front is available
				if(Board.getPieceAtLocation(possibleSpot) == null) possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
				
				//Handle Diagonals
				
				//pawn is all the way left
				if(column == 0) {
					possibleSpot[0] = row - 1;
					possibleSpot[1] = column + 1;
					if(Board.getPieceAtLocation(possibleSpot) != null && Board.getPieceAtLocation(possibleSpot).getColor() == 'b') 
						possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
				}
				//pawn is all the way right
				else if(column == 7) {
					possibleSpot[0] = row - 1;
					possibleSpot[1] = column - 1;
					if(Board.getPieceAtLocation(possibleSpot) != null && Board.getPieceAtLocation(possibleSpot).getColor() == 'b') 
						possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
				}
				//in the middle somewhere
				else if(column < 7 && column > 0) {
					possibleSpot[0] = row - 1;
					
					possibleSpot[1] = column + 1;
					if(Board.getPieceAtLocation(possibleSpot) != null && Board.getPieceAtLocation(possibleSpot).getColor() == 'b') possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
					
					possibleSpot[1] = column - 1;
					if(Board.getPieceAtLocation(possibleSpot) != null && Board.getPieceAtLocation(possibleSpot).getColor() == 'b') possibleMoves.add(new int[] {possibleSpot[0], possibleSpot[1]});
					
				}
				
			}
		}
		
		return possibleMoves;
	}
	

}
