/**
 * 
 * 
 * @author il166, yuc1
 * 
 * 
 * 
 */

package Pieces;

import java.util.ArrayList;

import board.Board;

public class Queen extends ChessPiece{
	public Queen(char color, int row, int column){
		super(row,column,color);

	}
			
	/**
	 * ToStrings the object Queen
	 * @return String of the object
	 */
	public String toString() {
		return super.getColor()+"Q";
		
	}

	/**
	 * Gets a list of all the possible moves that queen can make. 
	 * @return ArrayList<int[]> of all possible moves that piece can do. 
	 */
	@Override
	public ArrayList<int[]> validMoves() {
		int[] initialPos = super.getPosition();
		
		ArrayList<int[]> listOfMoves = new ArrayList<>();
		
		int row= initialPos[0];
		int column=initialPos[1];
		int currentRow, currentColumn;

		//check to the left of piece, and go down.
		for(currentRow=row+1 ,currentColumn=column-1;currentColumn>=0 && currentRow<=7; currentColumn--, currentRow++) {
			
			ChessPiece pieceAtLocation=Board.getPieceAtLocation(new int[] {currentRow,currentColumn});
				
			if(pieceAtLocation==null){
				listOfMoves.add(new int[] {currentRow, currentColumn});
			}else if(!(pieceAtLocation.getColor()==(super.getColor()))) {
				listOfMoves.add(new int[] {currentRow, currentColumn});
				break;
			}else {
				break;
			}
			
		}
		
		//check to the left of piece and go up.
		for(currentRow=row-1 ,currentColumn=column-1;currentColumn>=0 && currentRow>=0; currentColumn--, currentRow--) {

			ChessPiece pieceAtLocation=Board.getPieceAtLocation(new int[] {currentRow,currentColumn});
				
			if(pieceAtLocation==null){
				listOfMoves.add(new int[] {currentRow, currentColumn});
			}else if(!(pieceAtLocation.getColor()==(super.getColor()))) {
				listOfMoves.add(new int[] {currentRow, currentColumn});
				break;
			}else {
				break;
			}
		}
		
		//check to the right of piece and go up.
		for(currentRow=row-1 ,currentColumn=column+1;currentColumn<=7 && currentRow>=0; currentColumn++, currentRow--) {

			ChessPiece pieceAtLocation=Board.getPieceAtLocation(new int[] {currentRow,currentColumn});
				
			if(pieceAtLocation==null){
				listOfMoves.add(new int[] {currentRow, currentColumn});
			}else if(!(pieceAtLocation.getColor()==(super.getColor()))) {
				listOfMoves.add(new int[] {currentRow, currentColumn});
				break;
			}else {
				break;
			}
		}
		
		//check to the right of piece and go down.
		for(currentRow=row+1 ,currentColumn=column+1;currentColumn<=7 && currentRow<=7; currentColumn++, currentRow++) {

			ChessPiece pieceAtLocation=Board.getPieceAtLocation(new int[] {currentRow,currentColumn});
				
			if(pieceAtLocation==null){
				listOfMoves.add(new int[] {currentRow, currentColumn});
			}else if(!(pieceAtLocation.getColor()==(super.getColor()))) {
				listOfMoves.add(new int[] {currentRow, currentColumn});
				break;
			}else {
				break;
			}
		}
		//check to the under the piece.
		for(currentRow=row+1 ;currentRow<=7; currentRow++) {
			
			ChessPiece pieceAtLocation=Board.getPieceAtLocation(new int[] {currentRow,column});
				
			if(pieceAtLocation==null){
				listOfMoves.add(new int[] {currentRow, column});
			}else if(!(pieceAtLocation.getColor()==(super.getColor()))) {
				listOfMoves.add(new int[] {currentRow, column});
				break;
			}else {
				break;
			}
			
		}

		//check above the piece
		for(currentRow=row-1 ;currentRow>=0; currentRow--) {
			
			ChessPiece pieceAtLocation=Board.getPieceAtLocation(new int[] {currentRow,column});
				
			if(pieceAtLocation==null){
				listOfMoves.add(new int[] {currentRow, column});
			}else if(!(pieceAtLocation.getColor()==(super.getColor()))) {
				listOfMoves.add(new int[] {currentRow, column});
				break;
			}else {
				break;
			}
			
		}
		
		//check left of the piece
		for(currentColumn=column-1 ;currentColumn>=0; currentColumn--) {
			
			ChessPiece pieceAtLocation=Board.getPieceAtLocation(new int[] {row,currentColumn});
				
			if(pieceAtLocation==null){
				listOfMoves.add(new int[] {row, currentColumn});
			}else if(!(pieceAtLocation.getColor()==(super.getColor()))) {
				listOfMoves.add(new int[] {row, currentColumn});
				break;
			}else {
				break;
			}
			
		}
		
		//check right of the piece
		for(currentColumn=column+1 ;currentColumn<=7; currentColumn++) {
			
			ChessPiece pieceAtLocation=Board.getPieceAtLocation(new int[] {row,currentColumn});
				
			if(pieceAtLocation==null){
				listOfMoves.add(new int[] {row, currentColumn});
			}else if(!(pieceAtLocation.getColor()==(super.getColor()))) {
				listOfMoves.add(new int[] {row, currentColumn});
				break;
			}else {
				break;
			}
			
		}
		return listOfMoves;
		
	}
	

}
