

package chess;
import board.Board;
import java.util.*;

import Pieces.ChessPiece; 



public class Chess13 {
	
	
	public static int counter = 0;

	/**
	 * @author il166, yuc1
	 *  run main operations
	 * @param args -
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Board board= new Board();
		
		Scanner sc= new Scanner(System.in);
		
		boolean whitesTurn = true;
		boolean draw = false;

		while(true) {
			counter = 1;
			if(whitesTurn) {
				System.out.print("White's move: ");
				
			}
			else {
				System.out.print("Black's move: ");
				
			}

			
			
			String input = sc.nextLine();
			

			
			StringTokenizer inputTokens = new StringTokenizer(input, " "); 

			int numTokens=inputTokens.countTokens();

			//moving a piece
			if(numTokens==2) {
				
				String[] two_inputs = new String[2];
				
				
				
				for(int i=0;i<2;i++) {
					two_inputs[i] = inputTokens.nextToken();
				}
				if(Board.getPieceAtLocation(Board.translateToArray(two_inputs[0])) == null) {
                    System.out.println("Invalid Move");
                    continue;
                }

				if(whitesTurn == true && Board.getPieceAtLocation(Board.translateToArray(two_inputs[0])).getColor() == 'b') {
					System.out.println("Invalid Move");
					continue;
				}
				if(whitesTurn == false && Board.getPieceAtLocation(Board.translateToArray(two_inputs[0])).getColor() == 'w') {
					System.out.println("Invalid Move");
					continue;
				}
				if(two_inputs[0].length()!=2 && two_inputs[1].length()!=2) {
					System.out.println("Invalid Move");
					continue;
				}
				if(two_inputs[0].equals(two_inputs[1])) {
					System.out.println("Invalid Move");
					continue;
				}
				
				if(!Character.toString(two_inputs[0].charAt(0)).matches("[abcdefgh]") ){
					System.out.println("Invalid Move");
					continue;
				}
				
				if(!Character.toString(two_inputs[1].charAt(0)).matches("[abcdefgh]") ){
					System.out.println("Invalid Move");
					continue;
				}
				
				//check number of two inputs 0.
				if(Character.getNumericValue(two_inputs[0].charAt(1))>8 || Character.getNumericValue(two_inputs[0].charAt(1))<1 || two_inputs[0].substring(1,two_inputs[0].length()).length()>1) {
					System.out.println("Invalid Move");
					continue;
				}
				//check number of two inputs 1
				if(Character.getNumericValue(two_inputs[1].charAt(1))>8 || Character.getNumericValue(two_inputs[1].charAt(1))<1 || two_inputs[1].substring(1,two_inputs[0].length()).length()>1) {
					System.out.println("Invalid Move");
					continue;
				}
				
				boolean validMove = board.checkMoveValidity(Board.getPieceAtLocation(Board.translateToArray(two_inputs[0])),two_inputs[1]);
				
				
				
				
				if(validMove) {
					boolean p = board.checkPromotion(two_inputs[0], two_inputs[1], "Q", Board.getPieceAtLocation(Board.translateToArray(two_inputs[0])).getColor());
					if (!p)
						board.movePiece(two_inputs[0],two_inputs[1]);
				}else {
					System.out.println("Invalid Move");
					continue;
				}
				
				char color;
				if(Board.getPieceAtLocation(Board.translateToArray(two_inputs[1])).getColor()=='b') {
					color='w';
				}else {
					color='b';
				}
				
				if(Board.isKingChecked(color,Board.board)) {
					char color2;
					if(whitesTurn)
						color2='w';
					else
						color2='b';

					if(Board.isKingCheckMated(color2)) {
						if(color2=='w')
							System.out.println("White wins");
						else
							System.out.println("Black wins");
						break;
					}else {
						System.out.println("Check");
					}
					System.out.println();
				}
				
			//user requests draw or promotion
			}else if(numTokens==3){
				
				String[] three_inputs = new String[3];
				for(int i=0;i<3;i++) {
					three_inputs[i] = inputTokens.nextToken();
				}
				

				String origin = three_inputs[0];
				String destination = three_inputs[1];
				String input3 = three_inputs[2];
				
				if(Board.getPieceAtLocation(Board.translateToArray(origin)) == null) {
                    System.out.println("Invalid Move");
                    continue;
                }
				
				//PROMOTION
				if(input3.equals("N") || input3.equals("Q") || input3.equals("B") || input3.equals("R")) {
					System.out.println("PROMORTION");
					boolean validMove = board.checkMoveValidity(Board.getPieceAtLocation(Board.translateToArray(origin)), destination);
					if(validMove) {
						boolean promotion = board.checkPromotion(origin, destination, input3, Board.getPieceAtLocation(Board.translateToArray(origin)).getColor());
						if(promotion == false) {
							System.out.println("Invalid Input");
							continue;
						}
						else {
							
						}
					}
				}
				
				else if(input3.equals("draw?")) {
					
					if(Board.getPieceAtLocation(Board.translateToArray(origin)) == null) {
	                    System.out.println("Invalid Move");
	                    continue;
	                }

					if(whitesTurn == true && Board.getPieceAtLocation(Board.translateToArray(origin)).getColor() == 'b') {
						System.out.println("Invalid Move");
						continue;
					}
					if(whitesTurn == false && Board.getPieceAtLocation(Board.translateToArray(origin)).getColor() == 'w') {
						System.out.println("Invalid Move");
						continue;
					}
					if(origin.length()!=2 && destination.length()!=2) {
						System.out.println("Invalid Move");
						continue;
					}
					if(origin.equals(destination)) {
						System.out.println("Invalid Move");
						continue;
					}
					
					if(!Character.toString(origin.charAt(0)).matches("[abcdefgh]") ){
						System.out.println("Invalid Move");
						continue;
					}
					
					if(!Character.toString(destination.charAt(0)).matches("[abcdefgh]") ){
						System.out.println("Invalid Move");
						continue;
					}
					
					//check number of two inputs 0.
					if(Character.getNumericValue(origin.charAt(1))>8 || Character.getNumericValue(origin.charAt(1))<1 || origin.substring(1,origin.length()).length()>1) {
						System.out.println("Invalid Move");
						continue;
					}
					//check number of two inputs 1
					if(Character.getNumericValue(destination.charAt(1))>8 || Character.getNumericValue(destination.charAt(1))<1 || destination.substring(1,destination.length()).length()>1) {
						System.out.println("Invalid Move");
						continue;
					}
					
					boolean validMove = board.checkMoveValidity(Board.getPieceAtLocation(Board.translateToArray(origin)),destination);
					
					
					
					if(validMove) {
						boolean promotion = board.checkPromotion(origin, destination, "Q", Board.getPieceAtLocation(Board.translateToArray(origin)).getColor());
						if (!promotion) board.movePiece(origin,destination);
					}else {
						System.out.println("Invalid Move");
						continue;
					}
					
					char color;
					if(Board.getPieceAtLocation(Board.translateToArray(destination)).getColor()=='b') {
						color='w';
					}else {
						color='b';
					}
					
					if(Board.isKingChecked(color,Board.board)) {
						char color2;
						if(whitesTurn)
							color2='w';
						else
							color2='b';
						
						if(Board.isKingCheckMated(color2)) {
							if(color2=='w')
								System.out.println("White wins");
							else
								System.out.println("Black wins");
							break;
						}else {
							System.out.println("Check");
						}
						System.out.println();
					}
					
					draw = true;
					
				}
				
				else {
					System.out.println("Invalid Move");
					continue;
				}
				
				
				
				

			//user forfeits
			}else if(numTokens==1) {


				String oneInput = inputTokens.nextToken();

				if(!oneInput.equals("resign") && !oneInput.equals("draw")) {
					System.out.println("Invalid Move");
					continue;
				}
				else if(oneInput.equals("resign")){
					if(whitesTurn) {
						System.out.println("Black wins");
						break;
					}
					else {
						System.out.println("White wins");
						break;
					}
					
				}else if(oneInput.equals("draw") && draw) {
					//System.out.println("draw");
					break;
				}else {
					System.out.println("Invalid Move");
					continue;
				}

			}
			
			else if(numTokens == 4) {
				String[] four_inputs = new String[4];
				for(int i=0;i<4;i++) {
					four_inputs[i] = inputTokens.nextToken();
				}
				
				String origin = four_inputs[0];
				String destination = four_inputs[1];
				String promotion = four_inputs[2];
				String drawString = four_inputs[3];
				
				
				if(whitesTurn == true && Board.getPieceAtLocation(Board.translateToArray(origin)).getColor() == 'b') {
					System.out.println("Invalid Move");
					continue;
				}
				if(whitesTurn == false && Board.getPieceAtLocation(Board.translateToArray(origin)).getColor() == 'w') {
					System.out.println("Invalid Move");
					continue;
				}
				if(origin.length()!=2 && origin.length()!=2) {
					System.out.println("Invalid Move");
					continue;
				}
				if(origin.equals(destination)) {
					System.out.println("Invalid Move");
					continue;
				}
				
				if(!Character.toString(origin.charAt(0)).matches("[abcdefgh]") ){
					System.out.println("Invalid Move");
					continue;
				}
				
				if(!Character.toString(destination.charAt(0)).matches("[abcdefgh]") ){
					System.out.println("Invalid Move");
					continue;
				}
				
				//check number of two inputs 0.
				if(Character.getNumericValue(origin.charAt(1))>8 || Character.getNumericValue(origin.charAt(1))<1 || origin.substring(1,origin.length()).length()>1) {
					System.out.println("Invalid Move");
					continue;
				}
				//check number of two inputs 1
				if(Character.getNumericValue(destination.charAt(1))>8 || Character.getNumericValue(destination.charAt(1))<1 || destination.substring(1,origin.length()).length()>1) {
					System.out.println("Invalid Move");
					continue;
				}
				
				if(drawString.equals("draw?") && (promotion.equals("Q") || promotion.equals("N") || promotion.equals("R") || promotion.equals("B"))) {
					
					boolean validMove = board.checkMoveValidity(Board.getPieceAtLocation(Board.translateToArray(origin)), destination);
					if(validMove) {
						boolean promote = board.checkPromotion(origin, destination, promotion, Board.getPieceAtLocation(Board.translateToArray(origin)).getColor());
						if(promote == false) {
							System.out.println("Invalid Input");
							continue;
						}
						else {
							draw = true;
						}
					}
					
					
				}
			}
			
			else {
				System.out.println("Invalid Move");
				continue;
			}
			
			if(whitesTurn) whitesTurn = false;
			else whitesTurn = true;
			

		}
		sc.close();
	}
}
